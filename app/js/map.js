jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyA9Way76sbIfbZLroJO4jxMu-2ZjpBqESA&sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers_click = [];
    var markers = [
        ['KLIMT', -12.102583, -77.044529]
        /*['Astrid & Gastón', -12.0965675,-77.034959],
        ['Country Club Lima Hotel', -12.098159,-77.048715],
        ['Malabar', -12.0943144,-77.0347967],
        ['Osaka', -12.1064659,-77.0385594],
        ['Vivaldi', -12.0981589,-77.036525],
        ['CCPUCP', -12.1045979,-77.0387267],
        ['Casa de la Cultura', -12.099013,-77.034666],
        ['Galería Enlace Arte Contemporáneo', -12.1053492,-77.0388633],
        ['Museo Marina Núñez del Prado', -12.1010996,-77.0338367]*/
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>KLIMT</h3>' +
        '<p>Av. Pezet cuadra 5 (esquina con Gral. Muñiz)</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Astrid & Gastón</h3>' +
        '<p>Av. Paz Soldán 290, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Country Club Lima Hotel</h3>' +
        '<p>Calle Los Eucaliptos 590, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Malabar</h3>' +
        '<p>Camino Real 101, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Osaka</h3>' +
        '<p>Av. Pardo y Aliaga 660, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Vivaldi</h3>' +
        '<p>Av. Camino Real 415, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>CCPUCP</h3>' +
        '<p>Av. Camino Real 1075, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Casa de la Cultura</h3>' +
        '<p>Calle Los Incas 270, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Galería Enlace Arte Contemporáneo</h3>' +
        '<p>Av. Camino Real 1123, San Isidro</p>' + 
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Museo Marina Núñez del Prado</h3>' +
        '<p>Calle Antero Aspillaga N° 300, San Isidro</p>' + 
        '</div>']
    ];

    var image = [
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-klimt-datos.png',
            scaledSize: new google.maps.Size(250, 184)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-1.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-2.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-3.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-4.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-5.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-6.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-7.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-8.png',
            scaledSize: new google.maps.Size(80, 80)
        },
        {
            url: 'http://clientes.caleidosmedia.com/klimt/app/img/marker-9.png',
            scaledSize: new google.maps.Size(80, 80)
        }
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);

        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            icon: image[i]
        });
        
        // Allow each marker to have an info window    
        /*google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));*/

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);

        markers_click.push(marker);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(17);
        google.maps.event.removeListener(boundsListener);
    });


    /*$(".sidebar__click").on("click", function (e) {
        var id = $(this).data("box");
        var num = id.substr(id.length - 1);
        var lat = markers[num][1];
        var lng = markers[num][2];

        map.setCenter(new google.maps.LatLng( lat, lng ) );
        map.setZoom(20);
        infoWindow.setContent(infoWindowContent[num][0]);
        infoWindow.open(map, markers_click[num]);

        e.preventDefault();
    });*/
}