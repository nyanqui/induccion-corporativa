$(document).ready(function(){

    var durante8 = function () {
        var rptas = ["1", "4", "8", "11", "15", "18"];
        var contestadas = [null, null, null, null, null, null];

        var validate = function () {
            var totalRptas = rptas.length;
            var rptasContestadas = 0;

            $(".durante8 .content-btn").each( function (i) {
                if ($(this).find(".active-btn-green").length) {
                    var rpta = $(this).find(".active-btn-green").data("value");
                    rptasContestadas++;
                    
                    if (rpta == rptas[i]) {
                        contestadas[i] = true;
                    } else {
                        contestadas[i] = false;
                    }

                        console.log(contestadas);
                    if (totalRptas == rptasContestadas) {
                        var correcto = true;

                        $.each( contestadas, function( key, value ) {
                            if (!value) {
                                correcto = false;
                                return false;
                            }
                        });

                        if (correcto) {
                            location.assign("feedback-antes-correcta.html");
                        } else {
                            location.assign("feedback-antes-incorrecta.html");
                        }
                    }
                }
            });
        };

        $(".content-btn .btn").on("click", function (e) {
            var val = $(this).data("value");
            $(this).siblings().removeClass("active-btn-green");
            $(this).addClass("active-btn-green");

            validate();

            e.preventDefault();
        });
    };

    durante8();
});



